package com.example.tecnoglass.demo.controller;

import com.example.tecnoglass.demo.constants.UtilsConstants;
import com.example.tecnoglass.demo.entity.Customer;
import com.example.tecnoglass.demo.model.OrderModel;
import com.example.tecnoglass.demo.model.States;
import com.example.tecnoglass.demo.service.CustomerService;
import com.example.tecnoglass.demo.service.OrderService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

// TODO: Auto-generated Javadoc

/**
 * The Class StudentController.
 */
@RestController
@RequestMapping("/order")
public class OrderController {
	
	/** The Constant LOG. */
	private static final Log LOG = LogFactory.getLog(OrderController.class);
	
	/** The order service. */
	@Autowired
	@Qualifier("orderServiceImpl")
	private OrderService orderService;

	/** The customer service. */
	@Autowired
	@Qualifier("customerServiceImpl")
	private CustomerService customerService;
	/**
	 * Adds the.
	 *
	 * @param orderModel the order model
	 * @return the response entity
	 */
	@PostMapping("")
	public ResponseEntity<OrderModel> add(@RequestBody OrderModel orderModel){
		LOG.info("Method: add() -- PARAMS:"+ orderModel.toString());
		Customer customer = customerService.findById(orderModel.getCustomer().getId());
		orderModel.setState(States.Solicitada);
		orderModel.setCustomer(customer);
		OrderModel order = orderService.add(orderModel);
		return new ResponseEntity<OrderModel>(order, HttpStatus.CREATED);
	} 
	
	/**
	 * Approve.
	 *
	 * @param number the number
	 * @return the response entity
	 */
	@PutMapping("/approve")
	public ResponseEntity<Object> approve( @RequestParam(name="number", required=true) long number){
		LOG.info("Method: approve() -- PARAMS:"+ number);
		OrderModel order;
		try {
			order = orderService.findModelByNumber(number);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return new ResponseEntity<Object>(UtilsConstants.ORDER_ERROR_UPDATED, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(order != null){
			order.setState(States.Aprobada);
			OrderModel update_order = orderService.add(order);
			return new ResponseEntity<Object>(update_order, HttpStatus.OK);
		}else{
			return new ResponseEntity<Object>(UtilsConstants.ORDER_ERROR_CREATED, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Cancel.
	 *
	 * @param number the number
	 * @return the response entity
	 */
	@PutMapping("/cancel")
	public ResponseEntity<Object> cancel( @RequestParam(name="number", required=true) long number){
		LOG.info("Method: cancel() -- PARAMS:"+ number);
		OrderModel order;
		try {
			order = orderService.findModelByNumber(number);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			return new ResponseEntity<Object>(UtilsConstants.ORDER_ERROR_UPDATED, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		if(order != null){
			order.setState(States.Anulada);
			OrderModel update_order = orderService.add(order);
			return new ResponseEntity<Object>(update_order, HttpStatus.OK);
		}else{
			return new ResponseEntity<Object>(UtilsConstants.ORDER_ERROR_CREATED, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * List all.
	 *
	 * @return the response entity
	 */
	@GetMapping("")
	public ResponseEntity<List<OrderModel>> listAll(){
		LOG.info("Method: listAll() ");
		orderService.listAll();
		return new ResponseEntity<List<OrderModel>>(new ArrayList<>(orderService.listAll()), HttpStatus.OK);
	}

}

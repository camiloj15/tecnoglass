package com.example.tecnoglass.demo.controller;

import com.example.tecnoglass.demo.constants.UtilsConstants;
import com.example.tecnoglass.demo.model.CustomerModel;
import com.example.tecnoglass.demo.service.CustomerService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

// TODO: Auto-generated Javadoc

/**
 * The Class StudentController.
 */
@RestController
@RequestMapping("/customer")
public class CustomerController {
	
	/** The Constant LOG. */
	private static final Log LOG = LogFactory.getLog(CustomerController.class);
	
	/** The customer service. */
	@Autowired
	@Qualifier("customerServiceImpl")
	private CustomerService customerService;

	/**
	 * Adds the.
	 *
	 * @param customerModel the customer model
	 * @return the response entity
	 */
	@PostMapping("")
	public ResponseEntity<CustomerModel> add(@RequestBody CustomerModel customerModel){
		LOG.info("Method: add() -- PARAMS:"+ customerModel.toString());
		CustomerModel customer = customerService.add(customerModel);
		return new ResponseEntity<CustomerModel>(customer, HttpStatus.CREATED);
	}
	
	/**
	 * Update.
	 *
	 * @param customerModel the customer model
	 * @param id the id
	 * @return the response entity
	 */
	@PutMapping("")
	public ResponseEntity<Object> update(@RequestBody CustomerModel customerModel, @RequestParam(name="id", required=true) int id){
		LOG.info("Method: update() -- PARAMS:"+ customerModel.toString());
		CustomerModel customer = customerService.findModelById(id);
		if(customer != null){
			customerModel.setId(id);
			CustomerModel update_student = customerService.add(customerModel);
			return new ResponseEntity<Object>(update_student, HttpStatus.OK);
		}else{
			return new ResponseEntity<Object>(UtilsConstants.CUSTOMER_ERROR_CREATED, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Delete.
	 *
	 * @param id the id
	 * @return the response entity
	 */
	@DeleteMapping("")
	public ResponseEntity<String> delete(@RequestParam(name="id", required=true) int id){
		LOG.info("Method: delete() -- PARAMS: id="+ id);
		try{
			customerService.remove(id);
		return new ResponseEntity<String>(UtilsConstants.CUSTOMER_DELETED, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<String>(UtilsConstants.CUSTOMER_ERROR_DELETED, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * List all.
	 *
	 * @return the response entity
	 */
	@GetMapping("")
	public ResponseEntity<List<CustomerModel>> listAll(){
		LOG.info("Method: listAll() ");
		customerService.listAll();
		return new ResponseEntity<List<CustomerModel>>(new ArrayList<>(customerService.listAll()), HttpStatus.OK);
	}

}

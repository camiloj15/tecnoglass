package com.example.tecnoglass.demo.entity;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

// TODO: Auto-generated Javadoc

/**
 * The Class Customer.
 */
@Entity
@Table(name = "customers")
public class Customer {

	/** The id. */
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id", unique=true, nullable=false)
	private long id;
	
	/** The name. */
	@Column(name="name", nullable=false)
	private String name;

	/** The address. */
	@Column(name="address", nullable=false)
	private String address;

	/** The cellphone. */
	@Column(name="cellphone", nullable=false)
	private String cellphone;

	/** The country. */
	@Column(name="country", nullable=false)
	private String country;

	/** The email. */
	@Column(name="email", nullable=false)
	private String email;

	/** The orders. */
	@OneToMany(fetch=FetchType.EAGER, mappedBy="customer")
	private Set<Order> orders = new HashSet<Order>();

	/**
	 * Instantiates a new customer.
	 */
	public Customer() {
	}

	/**
	 * Instantiates a new customer.
	 *
	 * @param name the name
	 * @param addres the addres
	 * @param cellphone the cellphone
	 * @param country the country
	 * @param email the email
	 * @param orders the orders
	 */
	public Customer(String name, String addres, String cellphone, String country, String email, Set<Order> orders) {
		this.name = name;
		this.address = addres;
		this.cellphone = cellphone;
		this.country = country;
		this.email = email;
		this.orders = orders;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * Sets the id.
	 *
	 * @param id the new id
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the address.
	 *
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Sets the address.
	 *
	 * @param addres the new address
	 */
	public void setAddress(String addres) {
		this.address = addres;
	}

	/**
	 * Gets the cellphone.
	 *
	 * @return the cellphone
	 */
	public String getCellphone() {
		return cellphone;
	}

	/**
	 * Sets the cellphone.
	 *
	 * @param cellphone the new cellphone
	 */
	public void setCellphone(String cellphone) {
		this.cellphone = cellphone;
	}

	/**
	 * Gets the country.
	 *
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Sets the country.
	 *
	 * @param country the new country
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Sets the email.
	 *
	 * @param email the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Gets the orders.
	 *
	 * @return the orders
	 */
	public Set<Order> getOrders() {
		return orders;
	}

	/**
	 * Sets the orders.
	 *
	 * @param orders the new orders
	 */
	public void setOrders(Set<Order> orders) {
		this.orders = orders;
	}
}

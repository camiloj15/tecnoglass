package com.example.tecnoglass.demo.entity;

import com.example.tecnoglass.demo.model.States;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

// TODO: Auto-generated Javadoc
/**
 * Created by camiloj15 on 23/08/17.
 */
@Entity
@Table(name = "orders")
public class Order {

    /** The number. */
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="number", unique=true, nullable=false)
    private long number;

    /** The customer. */
    @ManyToOne(fetch= FetchType.LAZY)
    @Cascade(org.hibernate.annotations.CascadeType.MERGE)
    @JoinColumn(name="customer_id")
    private Customer customer;

    /** The items. */
    @OneToMany(mappedBy="order", cascade = CascadeType.PERSIST)
    private Set<Item> items = new HashSet<Item>();

    /** The date. */
    @Column(name="address", nullable=false)
    private Date date;

    /** The state. */
    @Enumerated(EnumType.STRING)
    private States state;

    /**
     * Instantiates a new order.
     */
    public Order() {
    }

    /**
     * Instantiates a new order.
     *
     * @param customer the customer
     * @param items the items
     * @param date the date
     * @param state the state
     */
    public Order(Customer customer, Set<Item> items, Date date, States state) {
        this.customer = customer;
        this.items = items;
        this.date = date;
        this.state = state;
    }

    /**
     * Gets the number.
     *
     * @return the number
     */
    public long getNumber() {
        return number;
    }

    /**
     * Sets the number.
     *
     * @param number the new number
     */
    public void setNumber(long number) {
        this.number = number;
    }

    /**
     * Gets the customer.
     *
     * @return the customer
     */
    public Customer getCustomer() {
        return customer;
    }

    /**
     * Sets the customer.
     *
     * @param customer the new customer
     */
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    /**
     * Gets the items.
     *
     * @return the items
     */
    public Set<Item> getItems() {
        return items;
    }

    /**
     * Sets the items.
     *
     * @param items the new items
     */
    public void setItems(Set<Item> items) {
        this.items = items;
    }

    /**
     * Gets the date.
     *
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * Sets the date.
     *
     * @param date the new date
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * Gets the state.
     *
     * @return the state
     */
    public States getState() {
        return state;
    }

    /**
     * Sets the state.
     *
     * @param state the new state
     */
    public void setState(States state) {
        this.state = state;
    }
}

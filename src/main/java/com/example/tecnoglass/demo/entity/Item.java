package com.example.tecnoglass.demo.entity;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;

// TODO: Auto-generated Javadoc
/**
 * Created by camiloj15 on 23/08/17.
 */
@Entity
@Table(name = "items")
public class Item {

    /** The id. */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id", unique=true, nullable=false)
    private long id;

    /** The order. */
    @ManyToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name="order_id")
    private Order order;

    /** The large. */
    @Column(name="large", nullable=false)
    private double large;

    /** The anchor. */
    @Column(name="anchor", nullable=false)
    private double anchor;

    /**
     * Instantiates a new item.
     */
    public Item() {
    }

    /**
     * Instantiates a new item.
     *
     * @param order the order
     * @param large the large
     * @param anchor the anchor
     */
    public Item(Order order, double large, double anchor) {
        this.order = order;
        this.large = large;
        this.anchor = anchor;
    }

    /**
     * Gets the id.
     *
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * Gets the order.
     *
     * @return the order
     */
    public Order getOrder() {
        return order;
    }

    /**
     * Sets the order.
     *
     * @param order the new order
     */
    public void setOrder(Order order) {
        this.order = order;
    }

    /**
     * Gets the large.
     *
     * @return the large
     */
    public double getLarge() {
        return large;
    }

    /**
     * Sets the large.
     *
     * @param large the new large
     */
    public void setLarge(double large) {
        this.large = large;
    }

    /**
     * Gets the anchor.
     *
     * @return the anchor
     */
    public double getAnchor() {
        return anchor;
    }

    /**
     * Sets the anchor.
     *
     * @param anchor the new anchor
     */
    public void setAnchor(double anchor) {
        this.anchor = anchor;
    }
}

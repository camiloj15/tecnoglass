package com.example.tecnoglass.demo.component;

import com.example.tecnoglass.demo.entity.Customer;
import com.example.tecnoglass.demo.model.CustomerModel;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

// TODO: Auto-generated Javadoc

/**
 * The Class CustomerConverter.
 */
@Component("customerConverter")
public class CustomerConverter {
	
	/**
	 * Convert model to entity.
	 *
	 * @param customerModel the customer model
	 * @return the customer
	 */
	public Customer convertModelToEntity(CustomerModel customerModel){
		Customer customer = new Customer();
		customer.setAddress(customerModel.getAddress());
		customer.setId(customerModel.getId());
		customer.setCellphone(customerModel.getCellphone());
		customer.setCountry(customerModel.getCountry());
		customer.setName(customerModel.getName());
		customer.setEmail(customerModel.getEmail());
		customer.setOrders(customerModel.getOrders());
		return customer;
	}

	/**
	 * Convert entity to model.
	 *
	 * @param customer the customer
	 * @return the customer model
	 */
	public CustomerModel convertEntityToModel(Customer customer){
		CustomerModel customerModel = new CustomerModel();
		customerModel.setAddress(customer.getAddress());
		customerModel.setId(customer.getId());
		customerModel.setCellphone(customer.getCellphone());
		customerModel.setCountry(customer.getCountry());
		customerModel.setName(customer.getName());
		customerModel.setEmail(customer.getEmail());
		customerModel.setOrders(customer.getOrders());
		return customerModel;	
	}
	
	/**
	 * Convert model list to entity list.
	 *
	 * @param customerModel the customer model
	 * @return the list
	 */
	public List<Customer> convertModelListToEntityList(List<CustomerModel> customerModel){
		List<Customer> customers = new ArrayList<Customer>();
		for(CustomerModel model: customerModel){
			customers.add(convertModelToEntity(model));
		}
		return customers;
	}
	
	/**
	 * Convert entity list to model list.
	 *
	 * @param customers the customers
	 * @return the list
	 */
	public List<CustomerModel> convertEntityListToModelList(List<Customer> customers){
		List<CustomerModel> customersModel = new ArrayList<CustomerModel>();
		for(Customer entity: customers){
			customersModel.add(convertEntityToModel(entity));
		}
		return customersModel;
	}


}

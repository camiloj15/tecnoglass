package com.example.tecnoglass.demo.component;

import com.example.tecnoglass.demo.entity.Order;
import com.example.tecnoglass.demo.model.OrderModel;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

// TODO: Auto-generated Javadoc

/**
 * The Class OrderConverter.
 */
@Component("orderConverter")
public class OrderConverter {
	
	/**
	 * Convert model to entity.
	 *
	 * @param orderModel the order model
	 * @return the order
	 */
	public Order convertModelToEntity(OrderModel orderModel){
		Order order = new Order();
		order.setCustomer(orderModel.getCustomer());
		order.setNumber(orderModel.getNumber());
		order.setItems(orderModel.getItems());
		order.setDate(orderModel.getDate());
		order.setState(orderModel.getState());
		return order;
	}

	/**
	 * Convert entity to model.
	 *
	 * @param order the order
	 * @return the order model
	 */
	public OrderModel convertEntityToModel(Order order){
		OrderModel orderModel = new OrderModel();
		orderModel.setCustomer((order.getCustomer()==null)?null:order.getCustomer());
		orderModel.setNumber(order.getNumber());
		orderModel.setItems((order.getItems()==null)?null:order.getItems());
		orderModel.setDate(order.getDate());
		orderModel.setState(order.getState());
		return orderModel;	
	}
	
	/**
	 * Convert model list to entity list.
	 *
	 * @param orderModel the order model
	 * @return the list
	 */
	public List<Order> convertModelListToEntityList(List<OrderModel> orderModel){
		List<Order> orders = new ArrayList<Order>();
		for(OrderModel model: orderModel){
			orders.add(convertModelToEntity(model));
		}
		return orders;
	}
	
	/**
	 * Convert entity list to model list.
	 *
	 * @param orders the orders
	 * @return the list
	 */
	public List<OrderModel> convertEntityListToModelList(List<Order> orders){
		List<OrderModel> ordersModel = new ArrayList<OrderModel>();
		for(Order entity: orders){
			ordersModel.add(convertEntityToModel(entity));
		}
		return ordersModel;
	}


}

package com.example.tecnoglass.demo.constants;

// TODO: Auto-generated Javadoc
/**
 * The Class UtilsConstants.
 */
public class UtilsConstants {
	
	/** The Constant CUSTOMER_CREATED. */
	//CUSTOMERS CONTROLLER
	public final static String CUSTOMER_CREATED = "Cliente agregado exitosamente";
	
	/** The Constant CUSTOMER_UPDATED. */
	public final static String CUSTOMER_UPDATED = "Cliente actualizado exitosamente";
	
	/** The Constant CUSTOMER_DELETED. */
	public final static String CUSTOMER_DELETED = "Cliente eliminado exitosamente";
	
	/** The Constant CUSTOMER_ERROR_DELETED. */
	public final static String CUSTOMER_ERROR_DELETED = "Cliente no encontrado";
	
	/** The Constant CUSTOMER_ERROR_CREATED. */
	public final static String CUSTOMER_ERROR_CREATED = "Error al crear el estudiante, favor completar los campos";
	
	/** The Constant ORDER_CREATED. */
	//ORDER CONTROLLER
	public final static String ORDER_CREATED = "Orden agregado exitosamente";
	
	/** The Constant ORDER_UPDATED. */
	public final static String ORDER_UPDATED = "Orden actualizado exitosamente";
	
	/** The Constant ORDER_DELETED. */
	public final static String ORDER_DELETED = "Orden eliminado exitosamente";
	
	/** The Constant ORDER_ERROR_DELETED. */
	public final static String ORDER_ERROR_DELETED = "Orden no encontrado";
	
	/** The Constant ORDER_ERROR_CREATED. */
	public final static String ORDER_ERROR_CREATED = "Error al crear la orden, favor completar los campos";

	/** The Constant ORDER_ERROR_CREATED. */
	public final static String ORDER_ERROR_UPDATED = "Error al actualizar la orden, numero de orden no encontrado";

}

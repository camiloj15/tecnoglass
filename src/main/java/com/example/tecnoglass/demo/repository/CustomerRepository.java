package com.example.tecnoglass.demo.repository;

import com.example.tecnoglass.demo.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

// TODO: Auto-generated Javadoc

/**
 * The Interface CustomerRepository.
 */
@Repository("customerRepository")
public interface CustomerRepository extends JpaRepository<Customer, Serializable> {

	/**
	 * Find by id.
	 *
	 * @param id the id
	 * @return the customer
	 */
	public abstract Customer findById(long id);

}

package com.example.tecnoglass.demo.repository;

import com.example.tecnoglass.demo.entity.Order;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

// TODO: Auto-generated Javadoc

/**
 * The Interface OrderRepository.
 */
@Repository("orderRepository")
public interface OrderRepository extends JpaRepository<Order, Serializable> {

	/**
	 * Find by id.
	 *
	 * @param number the number
	 * @return the order
	 */
	public abstract Order findByNumber(long number);

}

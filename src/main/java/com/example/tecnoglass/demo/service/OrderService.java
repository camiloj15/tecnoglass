package com.example.tecnoglass.demo.service;

import com.example.tecnoglass.demo.entity.Order;
import com.example.tecnoglass.demo.model.CustomerModel;
import com.example.tecnoglass.demo.model.OrderModel;

import java.util.List;

// TODO: Auto-generated Javadoc

/**
 * The Interface OrderService.
 */
public interface OrderService {

	/**
	 * Adds the.
	 *
	 * @param orderModel the order model
	 * @return the order model
	 */
	public abstract OrderModel add(OrderModel orderModel);
	
	/**
	 * List all.
	 *
	 * @return the list
	 */
	public abstract List<OrderModel> listAll();
	
	/**
	 * Find by number.
	 *
	 * @param number the number
	 * @return the order
	 */
	public abstract Order findByNumber(long number);
	
	/**
	 * Find model by number.
	 *
	 * @param number the number
	 * @return the order model
	 * @throws Exception the exception
	 */
	public abstract OrderModel findModelByNumber(long number)  throws Exception;
	
	/**
	 * Removes the.
	 *
	 * @param number the number
	 * @throws Exception the exception
	 */
	public abstract void remove(long number) throws Exception; 
	
	
}

package com.example.tecnoglass.demo.service;

import com.example.tecnoglass.demo.entity.Customer;
import com.example.tecnoglass.demo.model.CustomerModel;

import java.util.List;

// TODO: Auto-generated Javadoc

/**
 * The Interface CustomerService.
 */
public interface CustomerService {

	/**
	 * Adds the.
	 *
	 * @param customerModel the customer model
	 * @return the customer model
	 */
	public abstract CustomerModel add(CustomerModel customerModel);
	
	/**
	 * List all.
	 *
	 * @return the list
	 */
	public abstract List<CustomerModel> listAll();
	
	/**
	 * Find by id.
	 *
	 * @param id the id
	 * @return the customer
	 */
	public abstract Customer findById(long id);
	
	/**
	 * Find model by id.
	 *
	 * @param id the id
	 * @return the customer model
	 */
	public abstract CustomerModel findModelById(long id);
	
	/**
	 * Removes the.
	 *
	 * @param id the id
	 * @throws Exception the exception
	 */
	public abstract void remove(long id) throws Exception; 
	
}

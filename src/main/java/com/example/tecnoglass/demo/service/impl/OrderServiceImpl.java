package com.example.tecnoglass.demo.service.impl;

import com.example.tecnoglass.demo.component.OrderConverter;
import com.example.tecnoglass.demo.entity.Order;
import com.example.tecnoglass.demo.model.OrderModel;
import com.example.tecnoglass.demo.model.States;
import com.example.tecnoglass.demo.repository.OrderRepository;
import com.example.tecnoglass.demo.service.OrderService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

// TODO: Auto-generated Javadoc

/**
 * The Class OrderServiceImpl.
 */
@Service("orderServiceImpl")
public class OrderServiceImpl implements OrderService {

	/** The order repository. */
	@Autowired
	@Qualifier("orderRepository")
	private OrderRepository orderRepository;

	/** The order converter. */
	@Autowired
	@Qualifier("orderConverter")
	public OrderConverter orderConverter;

	/* (non-Javadoc)
	 * @see com.example.tecnoglass.demo.service.OrderService#add(com.example.tecnoglass.demo.model.OrderModel)
	 */
	@Override
	public OrderModel add(OrderModel orderModel) {
		Order order = orderRepository.save(orderConverter.convertModelToEntity(orderModel));
		return orderConverter.convertEntityToModel(order);
	}

	/* (non-Javadoc)
	 * @see com.example.tecnoglass.demo.service.OrderService#listAll()
	 */
	@Override
	public List<OrderModel> listAll() {
		return orderConverter.convertEntityListToModelList(orderRepository.findAll());
	}

	/* (non-Javadoc)
	 * @see com.example.tecnoglass.demo.service.OrderService#remove(long)
	 */
	@Override
	public void remove(long number) throws Exception{
		Order order = findByNumber(number);
		if (order != null) {
			orderRepository.delete(order);
		}else{
			throw new Exception();
		}
	}
	
	/* (non-Javadoc)
	 * @see com.example.tecnoglass.demo.service.OrderService#findByNumber(long)
	 */
	@Override
	public Order findByNumber(long number) {
		return orderRepository.findByNumber(number);
	}

	/* (non-Javadoc)
	 * @see com.example.tecnoglass.demo.service.OrderService#findModelByNumber(long)
	 */
	@Override
	public OrderModel findModelByNumber(long number)  throws Exception{
		// TODO Auto-generated method stub
		Order order = findByNumber(number);
		if (order != null) {
			return orderConverter.convertEntityToModel(findByNumber(number));
		}else{
			throw new Exception();
		}
	}

}

package com.example.tecnoglass.demo.service.impl;

import com.example.tecnoglass.demo.component.CustomerConverter;
import com.example.tecnoglass.demo.entity.Customer;
import com.example.tecnoglass.demo.model.CustomerModel;
import com.example.tecnoglass.demo.repository.CustomerRepository;
import com.example.tecnoglass.demo.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;

// TODO: Auto-generated Javadoc

/**
 * The Class CustomerServiceImpl.
 */
@Service("customerServiceImpl")
public class CustomerServiceImpl implements CustomerService {

	/** The customer repository. */
	@Autowired
	@Qualifier("customerRepository")
	private CustomerRepository customerRepository;

	/** The customer converter. */
	@Autowired
	@Qualifier("customerConverter")
	public CustomerConverter customerConverter;

	/* (non-Javadoc)
	 * @see com.api.nativapps.service.CustomerService#add(com.api.nativapps.model.CustomerModel)
	 */
	@Override
	public CustomerModel add(CustomerModel customerModel) {
		Customer customer = customerRepository.save(customerConverter.convertModelToEntity(customerModel));
		return customerConverter.convertEntityToModel(customer);
	}

	/* (non-Javadoc)
	 * @see com.api.nativapps.service.CustomerService#listAll()
	 */
	@Override
	public List<CustomerModel> listAll() {
		return customerConverter.convertEntityListToModelList(customerRepository.findAll());
	}

	/* (non-Javadoc)
	 * @see com.api.nativapps.service.CustomerService#remove(long)
	 */
	@Override
	public void remove(long id) throws Exception{
		Customer customer = findById(id);
		if (customer != null) {
			customerRepository.delete(customer);
		}else{
			throw new Exception();
		}
	}

	/* (non-Javadoc)
	 * @see com.api.nativapps.service.CustomerService#findById(long)
	 */
	@Override
	public Customer findById(long id) {
		return customerRepository.findById(id);
	}

	/* (non-Javadoc)
	 * @see com.api.nativapps.service.CustomerService#findModelById(long)
	 */
	@Override
	public CustomerModel findModelById(long id) {
		// TODO Auto-generated method stub
		return customerConverter.convertEntityToModel(findById(id));
	}

}

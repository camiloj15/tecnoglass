package com.example.tecnoglass.demo.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import com.example.tecnoglass.demo.entity.Customer;
import com.example.tecnoglass.demo.entity.Item;

// TODO: Auto-generated Javadoc
/**
 * The Class OrderModel.
 */
public class OrderModel {
	
	    /** The number. */
    	private long number;

	    /** The customer. */
    	private Customer customer;

	    /** The items. */
    	private Set<Item> items = new HashSet<Item>();

	    /** The date. */
    	private Date date;

	    /** The state. */
    	private States state;

	    /**
    	 * Instantiates a new order model.
    	 */
    	public OrderModel() {
	    }

	    /**
    	 * Instantiates a new order model.
    	 *
    	 * @param customer the customer
    	 * @param items the items
    	 * @param date the date
    	 * @param state the state
    	 */
    	public OrderModel(Customer customer, Set<Item> items, Date date, States state) {
	        this.customer = customer;
	        this.items = items;
	        this.date = date;
	        this.state = state;
	    }

	    /**
    	 * Gets the number.
    	 *
    	 * @return the number
    	 */
    	public long getNumber() {
	        return number;
	    }

	    /**
    	 * Sets the number.
    	 *
    	 * @param number the new number
    	 */
    	public void setNumber(long number) {
	        this.number = number;
	    }

	    /**
    	 * Gets the customer.
    	 *
    	 * @return the customer
    	 */
    	public Customer getCustomer() {
	        return customer;
	    }

	    /**
    	 * Sets the customer.
    	 *
    	 * @param customer the new customer
    	 */
    	public void setCustomer(Customer customer) {
	        this.customer = customer;
	    }

	    /**
    	 * Gets the items.
    	 *
    	 * @return the items
    	 */
    	public Set<Item> getItems() {
	        return items;
	    }

	    /**
    	 * Sets the items.
    	 *
    	 * @param items the new items
    	 */
    	public void setItems(Set<Item> items) {
	        this.items = items;
	    }

	    /**
    	 * Gets the date.
    	 *
    	 * @return the date
    	 */
    	public Date getDate() {
	        return date;
	    }

	    /**
    	 * Sets the date.
    	 *
    	 * @param date the new date
    	 */
    	public void setDate(Date date) {
	        this.date = date;
	    }

	    /**
    	 * Gets the state.
    	 *
    	 * @return the state
    	 */
    	public States getState() {
	        return state;
	    }

	    /**
    	 * Sets the state.
    	 *
    	 * @param state the new state
    	 */
    	public void setState(States state) {
	        this.state = state;
	    }

		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return "OrderModel [number=" + number + ", customer=" + customer + ", items=" + items + ", date=" + date
					+ ", state=" + state + "]";
		}
	    
	    

}

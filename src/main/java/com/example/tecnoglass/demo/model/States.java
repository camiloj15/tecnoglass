package com.example.tecnoglass.demo.model;

// TODO: Auto-generated Javadoc
/**
 * Created by camiloj15 on 23/08/17.
 */
public enum States {
    
    /** The Aprobada. */
    Aprobada,
    
    /** The Solicitada. */
    Solicitada,
    
    /** The Anulada. */
    Anulada
}
